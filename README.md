# 多场景+标签化API

#### 项目介绍
易优小程序是基于前端开源小程序+后端易优CMS+标签化API接口，是一套开源、快速搭建个性化需求的小程序CMS。轻量级TP底层框架，前后端分离，标签化API接口可对接所有小程序，支持二次开发。即使小白用户也能轻松搭建制作一套完整的线上版小程序。

```
如果对您有帮助，您可以点右上角 “Star” 收藏一下 ，获取第一时间更新，谢谢！
```
[点击这里了解标签化API接口教程文档](https://www.showdoc.com.cn/1419231451439365?page_id=6949289671916000)

[开源小程序QQ群：517743885](https://qm.qq.com/cgi-bin/qm/qr?k=Jji6_Gq9e0YcCLtsFQiQPxOmBz7cYyVV&jump_from=webapi)   加群请备注：易优

#### 最新更新
1. 增加：商城功能模块，支持购买，加入购物车，评价等等。
2. 增加：表单提交模块，支持栏目表单页面，可分栏目创建表单，前端自动调用表单字段。
3. 增加：会员中心模块，支持会员注册登录，商城订单，地址管理等等。
4. 增加：下载模型模块，支持下载栏目，详情，以及详情内的下载功能。
5. 增加：自定义模型模块，支持自定义模型栏目，详情。
7. 优化：小程序界面样式。
8. 注意：此小程序需要易优后台为1.5.9版本，并且需要覆盖最新补丁包，方可使用

#### 项目优点
1. 易用：十分钟搭建一个企业小程序，后台依赖成熟的易优CMS内容系统，简单易用。
2. 完善：易优CMS包含了一个常规企业网站需要的一切功能。
3. 扩展性：易优CMS可通过插件库支持更多功能，如短信或小程序等第三方扩展。
4. 丰富的资料：作为一个国内流行的cms，易优CMS拥有完善的帮助文档及标签手册。
5. 丰富的模版：易优拥有大量免费的漂亮模版，涵盖各行各业，任用户自由选择。

#### 扫码体验
![小程序-二维码](https://images.gitee.com/uploads/images/2021/0528/144804_54487b63_1283764.jpeg "小程序-二维码.jpg")

#### 小程序预览
![小程序界面预览](https://www.eyoucms.com/skin/gitee/144902_269d1f8d_4791449.png "小程序界面预览.png")

#### 后台
![后台-首页](https://images.gitee.com/uploads/images/2021/0527/201620_771f385b_1283764.png "后台-首页.png")
![后台-内容管理](https://images.gitee.com/uploads/images/2021/0527/201630_27e6cba9_1283764.png "后台-内容管理.png")
![后台-商城统计](https://images.gitee.com/uploads/images/2021/0527/201710_ff640ad2_1283764.png "后台-商城统计.png")
![后台-插件应用](https://images.gitee.com/uploads/images/2021/0610/155832_4b53ea19_1283764.png "后台-插件应用")

#### 标签化接口
1. 简单的配置，使用门槛超低。
![小程序-配置文件](https://images.gitee.com/uploads/images/2021/0527/173815_a5a6fa92_1283764.png "小程序-配置文件.png")
2. 标签化接口，完整手册指导，写接口就像套模板标签一样简单。
![小程序-API调用示例](https://images.gitee.com/uploads/images/2021/0527/174013_448bef00_1283764.png "小程序-API调用示例.png")

#### 项目演示

- 后台演示，用于管理发布内容、提供API接口等功能，可附带（PC/手机端页面）

    演示地址：https://demo.eyoucms.com/login.php

    下载地址：https://gitee.com/weng_xianhu/eyoucms

- 小程序演示，将下载的小程序源码导入微信开发者工具，即可实时预览。

    微信开发者工具：https://developers.weixin.qq.com/miniprogram/dev/devtools/stable.html

    ![微信开发者工具预览](https://images.gitee.com/uploads/images/2021/0528/104940_848cfe74_1283764.png "微信开发者工具预览.png")

#### 如何安装
##### 环境要求

- 服务器：Linux / Apache / IIS 
- PHP版本：5.4及5.4以上，支持php7.4等
- MYSQL版本：5.0以上，支持8.0等
- PS：php版本推荐php7以上系列，mysql推荐使用5.7+或以上

##### 一、后台端

1. 将易优CMS源码包上传解压至网站根目录 (支持安装在二级目录)
2. 在浏览器中输入您的站点域名
3. 根据页面提示，一步步完成安装即可
4. 后台安装完成后,点击"基本信息"->"接口配置"->"小程序API",点击配置
5. 进入配置页面后，填写微信登录下的AppID，AppSecret配置信息
![小程序信息配置](https://www.eyoucms.com/skin/gitee/20220721164246.png)

##### 二、小程序端
1. 将小程序源码导入微信开发者工具
2. 为了方便在工具里体验小程序，请勾上不校验域名
![小程序-不检验合法域名/https证书等选项](https://images.gitee.com/uploads/images/2021/0528/113811_12eec7ec_1283764.png "小程序-不检验合法域名/https证书等选项.png")
3. 点击根目录下的setting.js配置文件编辑，可将里面的域名修改为您的域名
![小程序-配置文件](https://images.gitee.com/uploads/images/2021/0528/112124_b9bcf75b_1283764.png "小程序-配置文件.png")
4. 至此一个简单的本地小程序已搭建完成

##### 三、发布上线
1. 先将后台端搭建在服务器/虚拟主机上
2. 登录微信官方网站，在【开发管理】->【开发设置】服务器域名里增加您的域名
![小程序-配置服务器域名](https://images.gitee.com/uploads/images/2021/0528/120400_deac5bdf_1283764.png "小程序-配置服务器域名.png")
3. 点击根目录下的setting.js配置文件编辑，可将里面的域名修改为您的域名
![小程序-配置文件](https://images.gitee.com/uploads/images/2021/0528/120529_8d794ca3_1283764.png "小程序-配置文件.png")
4. 在工具里点击预览，微信扫码体验
![小程序-工具预览体验](https://images.gitee.com/uploads/images/2021/0528/120618_c9964c36_1283764.png "小程序-工具预览体验.png")
5. 预览没问题之后，进行小程序的发布流程

    5.1) 通过工具上传代码
![小程序-点击上传](https://images.gitee.com/uploads/images/2021/0528/141334_0cedb557_1283764.png "小程序-点击上传.png")
![小程序-上传代码](https://images.gitee.com/uploads/images/2021/0528/141542_a3c615dd_1283764.png "小程序-上传代码.png")

    5.2) 前往微信小程序官网提交审核
![小程序-提交审核](https://images.gitee.com/uploads/images/2021/0528/142009_5aae5a45_1283764.png "小程序-提交审核.png")

    5.3) 微信收到审核通过消息之后，再前往官网正式发布小程序
![小程序-提交发布](https://images.gitee.com/uploads/images/2021/0528/144615_6a3865aa_1283764.png "小程序-提交发布.png")

6. 至此整个小程序+后端的搭建流程已完成，祝贺您成为一名合格的开发者


#### 参与贡献
1. 赞赞团队

